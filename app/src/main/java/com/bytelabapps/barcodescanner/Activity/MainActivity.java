package com.bytelabapps.barcodescanner.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bytelabapps.barcodescanner.Database.DatabaseHelper;
import com.bytelabapps.barcodescanner.Fragment.FragmentAboutUs;
import com.bytelabapps.barcodescanner.Fragment.ProductListFragment;
import com.bytelabapps.barcodescanner.Fragment.TabFragment;
import com.bytelabapps.barcodescanner.R;
import com.bytelabapps.barcodescanner.Utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.vision.barcode.Barcode;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private Context context;
    private Toolbar toolbar;
    public static final String BARCODE_KEY = "BARCODE";
    private final String TAG = MainActivity.class.getSimpleName();
    AppBarLayout mAppBarLayout;
    static final String AD_UNIT_ID = "ca-app-pub-8444650851472007/1386902745";
    AdView adView;
    LinearLayout linearLayout;
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DatabaseHelper(getApplicationContext());

        adView = new AdView(MainActivity.this);
        //adView.setAdUnitId(getString(R.string.ad_unit_id));
        adView.setAdUnitId(AD_UNIT_ID);
        adView.setAdSize(AdSize.BANNER);
        linearLayout = (LinearLayout) findViewById(R.id.add_linera);

        TabFragment tabFragment = new TabFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, tabFragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if (Utils.isNetworkAvailable(getApplicationContext())) {
            linearLayout.addView(adView);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice(AD_UNIT_ID)
                    .build();
            adView.loadAd(adRequest);
        } else {
            FrameLayout sv = (FrameLayout) findViewById(R.id.fragment_container);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            sv.setLayoutParams(layoutParams);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.scan) {
            Intent intent = new Intent(getApplicationContext(), FullScannerActivity.class);
            startActivity(intent);
        }
        if (id == R.id.rate) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=com.bytelabapps.barcodescanner"));
            startActivity(intent);
        }
        if (id == R.id.share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, " Barcode Scanner");
            String sAux = "\nBarcode Scanner\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.bytelabapps.barcodescanner \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select One"));
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.home) {
            TabFragment tabFragment = new TabFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, tabFragment);
            fragmentTransaction.commit();
        } else if (id == R.id.scan) {
            Intent intent = new Intent(getApplicationContext(), FullScannerActivity.class);
            startActivity(intent);
        }
        if (id == R.id.resetdata) {
            db.deleteAllProduct();
            ProductListFragment productListFragment = new ProductListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, productListFragment);
            fragmentTransaction.commit();
        } else if (id == R.id.rate) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=com.bytelabapps.barcodescanner"));
            startActivity(intent);
        } else if (id == R.id.feedback) {
            Intent email = new Intent(Intent.ACTION_SEND);
            email.setType("text/email");
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shahimtiyaj94@gmail.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Barcode Reader!");
            email.putExtra(Intent.EXTRA_TEXT, "Hello, " + "\n");
            startActivity(Intent.createChooser(email, "Send Feedback:"));
        } else if (id == R.id.share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, " Barcode Scanner");
            String sAux = "\nBarcode Scanner\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.bytelabapps.barcodescanner \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select One"));
        } else if (id == R.id.developer) {
            FragmentAboutUs aboutsFragment = new FragmentAboutUs();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, aboutsFragment);
            fragmentTransaction.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
